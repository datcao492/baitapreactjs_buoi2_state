import React, { Component } from "react";
import dataGlasses from "./dataGlasses.json";
import "./index.css";

export default class Glasses_state extends Component {
  state = {
    glassesName: "",
    glassesPrice: "",
    glassesDes: "",
    imgUrl: "",
  };

  handleChange = (id) => {
    let index = dataGlasses.findIndex((item) => {
      return item.id == id;
    });

    this.setState({
      imgUrl: dataGlasses[index].url,
      glassesName: dataGlasses[index].name,
      glassesPrice: "$" + dataGlasses[index].price,
      glassesDes: dataGlasses[index].desc,
    });
    document.getElementById("glassInfo").style.display = "block";
  };

  render() {
    return (
      <div className="bg">
        <h4 className="title">TRY YOUR GLASSES</h4>
        <div className="container">
          <div className="model">
            <div className="glassesPick">
              <img src={this.state.imgUrl} alt="" />
            </div>
            <div className="glassesInfor">
              <h5 className="glassesName text-center pl-2">
                {this.state.glassesName}
              </h5>
              <h6 className="glassesPrice text-center text-danger pl-2">
                {this.state.glassesPrice}
              </h6>
              <p className="glassesDesc text-center pl-2">
                {this.state.glassesDes}
              </p>
            </div>
          </div>
          <div className="row glassesList">
            {dataGlasses.map((item) => {
              return (
                <div className="col-2" key={item.id}>
                  <img
                    className="w-100"
                    src={item.url}
                    alt=""
                    onClick={() => {
                      this.handleChange(item.id);
                    }}
                  />
                </div>
              );
            })}
          </div>
        </div>
      </div>
    );
  }
}
